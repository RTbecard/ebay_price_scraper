#!/bin/bash

set -e

# rx 6700xt
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=300&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_stpos=12439&_sop=1&_udhi=900&_sadis=15&_from=R40&_nkw=%22rx%22%20%226700%22%20%22xt%22&LH_LocatedIn=77&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
./ebay-scraper.py amd_6700xt $url

# rx 6600 xt
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=200&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_stpos=12439&_sop=1&_udhi=900&_sadis=15&_from=R40&_nkw=%22rx%22%20%226600%22%20%22xt%22&LH_LocatedIn=77&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
./ebay-scraper.py amd_6600xt $url

# rx 6600
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=200&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&LH_ItemCondition=3000&_nkw=%22rx%22%20%226600%22%20-xt&LH_LocatedIn=77&rt=nc&LH_PrefLoc=1&_trksid=p2045573.m1684"
./ebay-scraper.py amd_6600 $url

# rtx 2060
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_stpos=12439&_sop=1&_udhi=900&_sadis=15&_from=R40&_nkw=%22rtx%22%20%222060%22%20-super&LH_LocatedIn=77&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
./ebay-scraper.py rtx2060 $url

# rtx 2060 super
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&LH_ItemCondition=3000&_nkw=%22rtx%22%20%222060%22%20%22super%22&LH_LocatedIn=77&rt=nc&LH_PrefLoc=1&_trksid=p2045573.m1684"
./ebay-scraper.py rtx2060super $url

# rtx 2070
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&_nkw=%22rtx%22%20%222070%22%20-super&LH_PrefLoc=1&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
./ebay-scraper.py rtx2070 $url

# rtx 2070 super
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&_nkw=%22rtx%22%20%222070%22%20%22super%22&LH_PrefLoc=1&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
./ebay-scraper.py rtx2070super $url

# rtx 2080 super
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&_nkw=%22rtx%22%20%222080%22%20%22super%22&LH_PrefLoc=1&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
./ebay-scraper.py rtx2080super $url

# rtx 2080
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&_nkw=%22rtx%22%20%222080%22%20-super&LH_PrefLoc=1&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
./ebay-scraper.py rtx2080 $url

# rtx 2080ti
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&LH_ItemCondition=3000&_nkw=%22rtx%22%20%222080%22%20%22ti%22&LH_LocatedIn=77&rt=nc&LH_PrefLoc=1&_trksid=p2045573.m1684"
./ebay-scraper.py rtx2080ti $url

# rtx 3060
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&LH_ItemCondition=3000&_nkw=%22rtx%22%20%223060%22%20-ti&LH_LocatedIn=77&rt=nc&LH_PrefLoc=1&_trksid=p2045573.m1684"
./ebay-scraper.py rtx_3060 $url

# rtx 3060 ti
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&_nkw=%22rtx%22%20%223060%22%20%22ti%22&LH_PrefLoc=1&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
./ebay-scraper.py rtx_3060ti $url

# trx 3070
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&_nkw=%22rtx%22%20%223070%22%20-ti&LH_PrefLoc=1&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
./ebay-scraper.py rtx_3070 $url

# rtx 3070 ti
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=150&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_sop=1&_udhi=900&_from=R40&_nkw=%22rtx%22%20%223070%22%20%22ti%22&LH_PrefLoc=1&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
./ebay-scraper.py rtx_3070ti $url

# rtx 3080
url="https://www.ebay.de/sch/i.html?_from=R40&_trksid=p2334524.m570.l1313&_nkw=%22rtx%22+%223080%22+-ti&_sacat=27386&LH_TitleDesc=0&_udlo=150&LH_Auction=1&rt=nc&LH_PrefLoc=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_ItemCondition=3000&LH_Sold=1&_dmd=1&_odkw=%22rtx%22+%223070%22+-ti&_osacat=27386&_dcat=27386&_sop=1&_udhi=900"
./ebay-scraper.py rtx_3080 $url

# rtx 3080 ti
url="https://www.ebay.de/sch/i.html?_from=R40&_trksid=p2334524.m570.l1313&_nkw=%22rtx%22+%223080%22+%22ti%22&_sacat=27386&LH_TitleDesc=0&_udlo=150&LH_Auction=1&LH_PrefLoc=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_odkw=%22rtx%22+%223080%22+-ti&_osacat=27386&_sop=1&_udhi=900"
./ebay-scraper.py rtx_3080ti $url

RScript ./make_plots.R
