This script will scrape prices from ebay's html search results and append any
new listings into a csv file.

Example usage:
```sh
# Install required python packages
pip3 install requests bs4 unicodedata pandas

# See help docs
python3 ./ebay-scraper.py -h

# Scrape data for RTX 6700 from ebay.de
url="https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_udlo=300&LH_Auction=1&_fosrp=1&_mPrRngCbx=1&_ipg=240&LH_Complete=1&LH_Sold=1&_dmd=1&_stpos=12439&_sop=1&_udhi=900&_sadis=15&_from=R40&_nkw=%22rx%22%20%226700%22%20%22xt%22&LH_LocatedIn=77&_dcat=27386&rt=nc&LH_ItemCondition=3000&_trksid=p2045573.m1684"
python3 ./ebay-scraper.py amd_6700xt $url
```

