#!/bin/python3

"""
This script is for scraping prices from ebay and saving them to a csv file.
it will:
 - Scrape prices for a set of search terms.
 - Append new prices to a csv file in the same folder.
"""

from os import path
import argparse
import re
import datetime
import pathlib
import locale

import requests
from bs4 import BeautifulSoup
import unicodedata

import pandas

# ========================= User global variables =============================
date_fmt = "%d. %b %Y"  # Format for ebays date display

# ------Fixed global variables
path_cur = pathlib.Path(path.abspath(__file__)).parent


# ------ Main function
def main():
    """
    Main function.  Holds all the parsing code.
    """
    global date_fmt
    # ------ Parse command line arguments
    parser = parser = argparse.ArgumentParser(
        description= """Ebay web scraper for storing search results of sold item
        listings. When no `search_url` is provided, a plot will display the
        monthly history of the specified item.""")
    parser.add_argument("search_name",
                        help="Specifies which CSV file the results will be saved in.",
                        nargs=1, type=str)
    parser.add_argument("search_url",
                        help="""Copy of the url holding ebay's sold listings
                        search results.  This is the page where prices will be
                        scraped from.""",
                        nargs=1, type=str)
    parser.add_argument("--locale",
                        help="""Locale to use to parse ebay's date strings.  For
                        example 'en_US.utf8'.  Defults to german local with
                        'de_DE.utf8'.  Incorrect locale will cause errors
                        reading listing dates.""",
                        default="de_DE.utf8",
                        type=str)
    parser.add_argument("--date_fmt",
                        help=f"""strptime formatting string for parsing listing
                        dates.  See
                        'https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes'.
                        Defaults to German: {date_fmt}""",
                        default=date_fmt,
                        type=str)
    args = parser.parse_args()
    name_file = args.search_name[0]
    url = args.search_url[0]

    # ------ Set locale and date format
    locale.setlocale(locale.LC_TIME, args.locale) # For reading german dates
    date_fmt = args.date_fmt

    name_csv = f"{name_file}.csv"
    file_csv = path.join(path_cur, name_csv)

    print("="*79)
    print("Results file: %s" % name_file)
    print("Locale: %s" % args.locale)
    print("Date format: %s" % date_fmt)
    print("url: %s" % url)

    scrape(url, file_csv)

def scrape(url, file_csv):
    """
    Code for scraping and appending to csv
    """

    # ------ Read previous data if exists
    if path.exists(file_csv):
        df_old = pandas.read_csv(file_csv)
        if "iid" in df_old.columns:
            ids = list(df_old['iid'].astype(str))
        else:
            ids = []
    else:
        ids = []

    # ------ Parse page and get listed items
    page = requests.get(url)
    #page.encoding = 'ISO_8859_1'
    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find("ul", class_="srp-results")
    items = results.findAll("li", class_ = "s-item", recursive=False)

    # ------ Loop items and get listing data
    listings = dict(iid = [], title = [], price = [], date = [])
    item = items[3]
    for item in items:

        try:
            href = item.find("a", recursive = True)['href']
            m = re.search('/itm/(.+?)\?', href)
            iid = m.group(1)
        except Exception as e:
            print("Missing listing id, skipping item.")
            continue

        # ------ Skip if this item id has previously been saved
        if iid in ids:
            continue

        try:
            title = item.find('span', {'role': 'heading'}).text
            price_str = item.find('span', class_="s-item__price").find("span").text.strip()
            # Convert price to numeric value
            price = float(re.sub("[^0-9.]", "", price_str)) / 100
            date_str = item.find('div', class_ = "s-item__title--tagblock").find("span").text.strip()
            date_str = re.sub("Verkauft", "", date_str).strip()
            # Convert to POSIX
            date = datetime.datetime.strptime(date_str, date_fmt)
            #date = date.replace(year = (datetime.datetime.now().year))

            # append to results
            listings['iid'].append(iid)
            listings['title'].append(title)
            listings['price'].append(price)
            listings['date'].append(date)
        except Exception as e:
            print("Scare failed for a item:")
            print(e)

    df = pandas.DataFrame(listings)

    with open(file_csv, 'a') as f:
        df.to_csv(f, mode='a', header=f.tell()==0, index = False)

    print(f"{len(df)} scraped prices appended to {path.basename(file_csv)}.")

def testing():
    """
    Dummy function holding parameters for testing this script.
    Not part of main program.
    """

    url = "https://www.ebay.de/sch/Grafik-Videokarten/27386/i.html?_from=R40&_fosrp=1&_nkw=6600+xt&_in_kw=4&_ex_kw=&_sacat=27386&LH_Sold=1&_mPrRngCbx=1&_udlo=300&_udhi=700&_samilow=&_samihi=&_sadis=15&_stpos=12439&_sargn=-1%26saslc%3D1&_fsradio2=%26LH_LocatedIn%3D1&_salic=77&LH_SubLocation=1&_sop=1&_dmd=1&_ipg=240&LH_Complete=1"
    name_csv = "amd_6600xt"
    file_csv = "amd_6600xt.csv"
    path_cur = "./"

if __name__ == "__main__":
    main()

