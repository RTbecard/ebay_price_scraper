require(ggplot2)
require(lubridate)
require(magrittr)

# Figure output settings
pdf.options(width = 11.7, height = 8.3)

files <- dir(pattern = ".csv")

df_plot <- data.frame()

file = files[1]
for (file in files){
  df = read.csv(file)
  name = strsplit(file, split = ".", fixed = T)[[1]][1]

  df_plot <- rbind(
    df_plot,
    data.frame(card = name, df[c("price", "date")]))
}

df_plot$posix <- as.POSIXct(df_plot$date)
df_plot$month <- as.Date(floor_date(df_plot$posix, 'month'))
df_plot$card <- factor(df_plot$card)

df_plot$group <- factor(paste0(df_plot$card, df_plot$month))

# ------ plot blxoplots per card
for(card_filt in unique(df_plot$card)){

  df_sub <- subset(df_plot, card == card_filt)
  limits <- quantile(df_sub$price, p = c(0.05, 0.95))

  plot <-   ggplot(df_sub) +
    geom_boxplot(aes(y = price, x = month, group = group)) +
    coord_cartesian(ylim = limits) +
    scale_x_date() +
    theme_bw() +
    ggtitle(card_filt) +
    labs(x = "Month", y = "Price (EUR)")
  print(plot)
}

# ------ Plot aggergate trends
df_agg <- aggregate(price ~ month + card, data = df_plot, FUN = 'median')
df_agg_labs <- aggregate(month ~ card, data = df_agg, FUN = 'max') %>%
  merge(df_agg, by = c("card", "month"))

ggplot(df_agg, aes(x = month, y = price, col = card)) +
  geom_line() +
  geom_label(data = df_agg_labs, aes(label = card)) +
  scale_x_date() +
  theme_bw() +
  labs(y = "Price (EUR)", x = "Month", color = "GPU") +
  ggtitle("Median prices for used GPUs, ebay.de") +
  theme(legend.position="bottom")
